# TheoInf 2
## Übungsgruppen Woche 3
### Aufgabe 1: Manipulaon einer Liste
Betrachten Sie folgenden Algorithmus, der auf einer Liste a[1…n] von n beliebigen natürlichen 
Zahlengestartet wird ($n \in \mathbb{N}$)
```python
algorithm algo1(a[1..n]):
    if i >= 2:
        while true: #Endlossschleife. endet irgendwann mit return
            for j in 2..n:
                if a[j-1] > a[j]:
                    a[j] = 2*a[j]
                    finished = false
            if finished:
                return #Algorithmus beenden
```
Wir übergeben dem Algorithmus algo1 die Liste a[1...8] = [3, 2, 4, 7, 1, 5, 4, 6]. Der Algorithmus verändert die
Zahlen in der Liste. Welche Zahlen stehen in a[1...8], wenn der Algorithmus beendet ist? Schreiben Sie die
Zahlen in der Reihenfolge auf, in der sie in der Liste stehen.

[3, 4, 4, 7, 2, 5, 8, 12], finished = false
[3, 4, 4, 7, 4, 5, 8, 12], finished = false
[3, 4, 4, 7, 8, 10, 16, 24], finished = false
[3, 4, 4, 7, 8, 10, 16, 24], finished = true
das heißt: am Ende ist a[1...8] = [3, 4, 4, 7, 8, 10, 16, 24]

### Aufgabe 2: Summe aller geraden Zahlen
Beschreiben Sie einen Algorithmus sumEven in Pseudocode, der die Summe aller geraden Zahlen aus einem
Array a[1…n] von n ganzen Zahlen berechnet. So soll z.B. sumEven([1, 11, 4, 1, 6, 4]) den Wert 4 + 6 + 4 = 14
zurückgeben; sumEven([1, 11, 5]) = 0. Zur Überprüfung, ob eine Zahl x gerade ist, können Sie dabei einfach
den Ausdruck "x ist gerade" verwenden; wir gehen davon aus, dass unser Maschinenmodell diesen Test als
Basisoperaon unterstützt.
```
algorithm sumEven(a[1..n]):
    sum = 0
    for i in 1..n:
        if a[i] ist gerade:
            sum=sum+a[i]
    return sum
```

### Aufgabe 3: Noch ein Sortieralgorithmus
Betrachten Sie folgenden Algorithmus, der ein Array a[1…n] von n Elementen sorert:
```
algorithm algo3(a[1..n]):
    if n = 1:
        return
    pos = 1
    while pos <= n:
        if(pos == 1)or(a[pos-1]<=a[pos]):
            pos=pos+1
        else:
            vertausche a[pos-1] mit a[pos] # (Zeile *)
            pos = pos-1
```
1. Wir starten algo3 auf dem Array a = [7, 2, 9, 3]. Geben Sie das Array a nach jeder Vertauschung von
zwei Elementen (Zeile *) an.  

[2, 7, 9, 3]
[2, 7, 3, 9]
[2, 3, 7, 9]


2. Geben Sie ein Array a[1...8] der Länge 8 an, bei dem die Laufzeit von algo3 maximal wird (worst-case
Beispiel).

a = [8, 7, 6, 5, 4, 3, 2, 1]


3. Welche asymptosche Laufzeit hat **algo3**? Geben Sie eine möglichst enge obere Schranke für die worst-
case Laufzeit (in Groß-O-Notaon) an.  
$O(n^2)$  

### Aufgabe 4: Palindrome erkennen
Ein Palindrom ist ein Wort, das rückwärts geschrieben genau so lautet wie vorwärts geschrieben, z.B.
"RENTNER". Sei a[1...n] eine Liste von n Buchstaben ($n \in \mathbb{N}$). Beschreiben Sie einen
Algorithmus isPalindrome in Pseudocode, der überprüft, ob die Eingabe a[1...n] einem Palindrom entspricht.
Zum Beispiel sollen isPalindrome(['R', 'E', 'N', 'T', 'N', 'E', 'R']) und isPalindrome(['A', 'N', 'N', 'A']) den
Wert true zurückliefern, wohingegen isPalindrome(['H', 'A', 'M', 'S', 'T', 'E', 'R']) den Wert false zurückliefern
soll.  

```
algorithm isPalindrom(a[1...n]):
    m = n/2, abgerundet
    if m >= 1:
        for i in 1..n:
            if a[i] != a[n-i+1]:
                return false
    return true
```
oder:
``` 
algorithm isPalindrom(a[1...n]):
    i = 1
    j = n
    while i < j:
        if a[i]!=a[j]:
            return false
        i = i+1
        j = j+1
    return true
```
oder auch:
```
algorithm isPalindrom(a[1...n]):
    b[1...n] ist ein Array
    j = n
    for i in 1..n:
        b[n] = a[i]
        j=j-1
    return a[1..n]==b[1..n] #vergleicht die Werte in a und b (mit for-Schleife)
```
### Aufgabe 5: Korrektheitsbeweise
Der folgende Algorithmus kann auf zwei beliebige natürliche Zahlen n, $m \in \mathbb{N}$ ausgeführt werden:
```
algorithm algo5(n, $m \in \mathbb{N}$):
    x=n
    y=m
    while x > 0:
        x=x-2
        y=y+10
    return y
```
1. Beweisen Sie: algo5(n, m) hält für alle Eingabeparameter n, $m \in \mathbb{N}$
**Antwort**:
x wird jeden Schleifendurchlauf um 2 kleiner, d.h. nach n/2 (aufgerundet) Schleifendurchläufen ist $x \leq 0$ und
die while-Schleife endet.

2. Beweisen Sie, dass 10*x + 2*y eine Schleifeninvariante von algo5 ist.
Antwort:
$x_i$ und $y_i$ seien die Werte der Variablen x und y nach dem i-ten Schleifendurchlauf, am Anfang sei $x_0 = n$ und
$y_0 = m$.
In der while-Schleife passiert Folgendes:  
$x_{i+1}= x_i − 2$  
$y_{i+1} = y_i + 10$  

$10 * x_{i+1} + 2 * y_{i+1} = 10 * (x_i − 2) + 2 · (y_i + 10) = 10 · x_i − 20 + 2 · y_i + 20 = 10 * x_i + 2 · y_i$
